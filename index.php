<?php

/**
* @param int $row
* @param int $pos
*
* @return float
*/
function W(int $row, int $pos) : float
{
    $blockWeight = 1;
    $temp = [];

    if(!blockExists($row, $pos)) {
        
        throw new Exception('Ivalid argument');
    }
    
    $temp = getBlockPressure($row, $pos, $blockWeight, $temp);
    
    return  $temp[$row][$pos] - $blockWeight;
}

/**
* @param int $row
* @param int $pos
* @param int $blockWeight
* @param array $temp
*
* @return float
*/
function getBlockPressure(int $row, int $pos, int $blockWeight, array $temp) : array {
    if(!blockExists($row, $pos)) {
        $temp[$row][$pos] = 0;
        return $temp;
    }
    
    if( isset($temp[$row][$pos]) ) {
        
        return $temp;
    }
    
    $temp = getBlockPressure(($row - 1), ($pos - 1), $blockWeight, $temp);
    $temp = getBlockPressure(($row - 1), ($pos), $blockWeight, $temp);
    $leftTopBlockPressure = $temp[$row - 1][$pos - 1];
    $rightTopBlockPressure = $temp[$row - 1][$pos];
    $topPressure = ( $leftTopBlockPressure + $rightTopBlockPressure ) / 2;
    $blockPressure = $blockWeight + $topPressure;
    
    $temp[$row][$pos] = $blockPressure;
    
    return $temp;
}

/**
* @param int $row
* @param int $pos
*
* @return bool
*/
function blockExists(int $row, int $pos) : bool
{
    if($row < 0 || $pos < 0 || $pos > $row) {
        
        return false;
    }
    
    return true;
}


echo W(322,156);

?>
